use serde::{Serialize, Deserialize};

// Field order of all structs mplicity linked to TSL JSON due to hyper's JSON deserialization.
// DO NOT CHANGE ORDER
#[derive(Serialize, Deserialize, Debug)]
pub struct EventInfo {
    id: String,
    event_id: String,
    organizer: String,
    name: String,
    venue: String,
    commentary_url: String,
    logo_url: String,
    styles_sheet: String,
    result_url: String,
    features: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RaceInfo {
    classes: Vec<ClassInfo>,
    classification: Vec<String>,
    duration: RaceDuration,
    fastest_lap: u32,
    race_id: String,
    race_name: String,
    planned_start_time: String,
    series_name: String,
    session_clock: RaceClock,
    session_flag: String,
    track: TrackInfo,
    track_conditions: String,
    track_type: u32, // No clue what this means
    units: u32, // No clue what this means
    weather_conditiosn: String, // Could be an enum if we had all the types.
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RaceClock {
    running: bool,
    time_to_go: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ClassInfo {
    color_id: String,
    class_id: String,
    name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SectorInfo{
    end_mark: u32,//Meters?
    is_speed_trap: bool,
    key: String,
    length: u32,// Meters?
    name: String,
    start: u32, //Meters?
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TrackInfo {
    display_name: String,
    length: u32, // Meters?
    name: String,
    sectors: Vec<SectorInfo>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RaceDuration {
    laps: u32,
    time: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum MessageType {
    KeepAlive = 6,
    
}

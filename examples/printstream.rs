use serde_json::{Result, Value};
use tungstenite::connect;
use tungstenite::Message;
use url::Url;
use tsl_rs::{EventInfo,RaceInfo};
//#[tokio::main]
fn main() {

    let event_id = "234531";
    // Need to get the HTML of the page
    //e.g. https://livetiming.tsl-timing.com/234421
    println!("Getting Event Page");
    //let resp = reqwest::blocking::get("https://livetiming.tsl-timing.com/234421").unwrap();
        //.json::<HashMap<String, String>>();
    // println!("{:?}",resp.text().unwrap());

    // Send POST https://livetiming.tsl-timing.com/results/live/negotiate?negotiateVersion=1
    // Configure Request Client
    // let client = reqwest::Client::new();
    //let resp = client.post("https://livetiming.tsl-timing.com/results/live/negotiate?negotiateVersion=1");

    // GET API
    // https://livetiming.tsl-timing.com/results/api/events/234421
    println!("Getting API Information for the event.");
    // let resp = reqwest::blocking::get("https://livetiming.tsl-timing.com/results/api/events/234421").unwrap().json::<EventInfo>().unwrap();
    let resp = reqwest::blocking::get(format!("https://livetiming.tsl-timing.com/results/api/events/{}", event_id)).unwrap().text().unwrap();
    println!("{:?}", resp);
    let json: EventInfo = serde_json::from_str(&resp).unwrap();
    

    // GET Race Data
    // https://livetiming.tsl-timing.com/results/api/sessions/234421/active
    print!("Getting initial race information for event.");
    let resp = reqwest::blocking::get(format!("https://livetiming.tsl-timing.com/results/api/sessions/{}/active", event_id)).unwrap().json::<RaceInfo>().unwrap();
    //let resp: Value = serde_json::from_str(&resp).unwrap();
    println!("{:?}",resp);

    // Open up the websocket with information obtained prior -- id gathered from the API response:
    // e.g., wss://livetiming.tsl-timing.com/results/live?id=rDa8paPG4ABYyS1LnHaNYQ
    // Save the event ID as tha twill be sent in the request messages for live timing
    // Then need to find the livetiming.tsl-timing.com link on the page
    // let event_id = "234421";
    // let mut ws = connect("wss://livetiming.tsl-timing.com/results/live?id=LhXJHB9I-7cCVJ0u9Gf5Mw").expect("Failed to connect to tsl-timing");
    // let mut ws = connect("wss://ws.postman-echo.com/raw").unwrap();
    // Configure stream

    // Send First message
    // {"protocol":"json","version":1}

    // Send Second Message... use ID from earlier
    //{"arguments":["234421"],"target":"registerForEvent","type":1}

    // These messages configure the websocket to send live timing
    /*
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetSessionData","A":["{}"],"I":0}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"RegisterConnectionId","A":["{}",true,true,true],"I":1}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetClasses","A":["{}"],"I":2}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetClassification","A":["{}"],"I":3}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"SendCurrentTrackingData","A":["{}"],"I":4}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetWeatherData","A":["{}"],"I":5}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetSessionStatistics","A":["{}"],"I":6}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetSessionData","A":["{}"],"I":7}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetIntermediatesSpeeds","A":["{}"],"I":8}}"#, event_id))).unwrap();
    ws.0.write_message(Message::Text(format!(r#"{{"H":"livetiming","M":"GetIntermediatesTimes","A":["{}"],"I":9}}"#, event_id))).unwrap();
    */
    println!("WebSocket handshake has been successfully completed");
/*
    loop {
        if ws.0.can_read() {
            println!("{}", ws.0.read_message().unwrap());
        }
        // Every KEEPALIVE received should be responded to with a KEEPALIVE

        // 
    }
    //let (write, read) 
*/
}

use serde_json::{Result, Value};
use std::collections::HashSet;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};

pub fn main() -> Result<()> {
    let file_name: Vec<String> = env::args().collect();
    let file_name = file_name[1].to_owned();

    let mut buf: String = String::new();
    let _reader = File::open(file_name).unwrap().read_to_string(&mut buf);

    // Find the first }
    let mut start: usize = find_start(&buf);
    let mut end: usize = find_end(&buf);

    let mut header_set: HashSet<String> = HashSet::new();
    loop {
        let v: Value = serde_json::from_str(&buf[start..end + 1])?;
        if let Some(msg) = v.get("M") {
            for packet in msg.as_array().unwrap() {
                if let Some(header) = packet.get("M") {
                    header_set.insert(header.to_string());
                }
            }
        }

        start = end + 1;
        end = start + find_end(&buf[start..]);

        if end >= buf.len() {
            break;
        } else {
            start = start + find_start(&buf[start..end + 1]);
        }
    }
    println!("{:?}", header_set);
    Ok(())
}

pub fn find_start(buf: &str) -> usize {
    // Need to skip all the `{}` 's
    let mut start: usize = 0;
    loop {
        if (&buf[start..start + 3] == "{}\n") {
            start = start + 3;
        } else {
            break;
        }
    }
    start
}

pub fn find_end(buf: &str) -> usize {
    if let Some(x) = buf.find("\n}\n") {
        x + 2
    } else {
        buf.len()
    }
}

# tsl-rs
Library handling the decoding of TSL data. This library is designed to support the ability
for endurance fans to gain a better understanding of the strategy(ies) being followed by
their favorite teams during races.

## TSL Websocket Format
Based on analysis, TSL leverages signalr to provide real time updates, at least with respect to their legacy
timing web interface. The information sent with an identifier field "I". 

Responses contain an "I" field with an identifier. It is assumed that this information in the response will
inform the client what data structure to deserialize to.

### JSON Fields
C: Unknown, appears to be a field with multiple tuples seperated by |.
M: The message body -- this is an array of other objects

### Message Body

H: Header (liveTiming )
M: Message Body ( updateClass, addUpdateFastLapHistory, RemoveAnimation, updateResult, addUpdateLeaderHistory, 
deleteLeaderRecord, updateSession, deleteFastLapRecord, updateSessionStatistics, mapAnimate, setTimeRemaining, 
ControlBroadcast, removeClass, removeCompetitor)

#### setTimeRemaining

Example:
```
        {
            "H": "liveTiming",
            "M": "setTimeRemaining",
            "A": [
                {
                    "d": [
                        "00",
                        "00",
                        "00"
                    ],
                    "r": false
                }
            ]
        },
```

Denotes the end of one racing session and the beginning of the next.

## Examples

### printstream.rs
User must add the GoDaddy Certificate Bundles - G2 (https://certs.godaddy.com/repository) to
/etc/pki/ca-trust/source/anchors/ . This is due to Rust using the system's certificate
store and GoDaddy, as well as most, Intermediate CA's.

### parse_tsl_json.rs
Explore the signalR packet structure.